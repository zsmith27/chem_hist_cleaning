---
title: "Compile historic chem"
author: "Gavin Lemley"
date: "December 18, 2019"
output: html_document
---

For combining EQuIS data (2001-2015), 2016, and 2017 data, then binding to existing 2018-2018 chem ITS tables. Script originally intended for just cleaning equis but added 2016 and 2017 inclusion. Old code at bottom for equis processing/invesitigation. 

# Prep

```{r Load libs and source dirs}
library(tidyverse)
library(lubridate)

root.dir <- rprojroot::find_root("chem_hist_cleaning.Rproj")
datamod.dir <- "C:/Users/gmlemley/New York State Office of Information Technology Services/SMAS - Streams Data Modernization"
tobecleaned.dir <- file.path(datamod.dir, "to_be_cleaned", "chem_2001_to_2017")
cleaned.dir <- file.path(datamod.dir, "Cleaned Files")

```

```{r Load source data and format}
result.equis <- read.csv(file.path(tobecleaned.dir,  "equis_results_history.csv"), stringsAsFactors = FALSE) %>% 
  rename_all(tolower) %>%
  rename(sample_del_grp_equis = lab_sdg,
         result_value = result_numeric) 

sample.equis <- read.csv(file.path(tobecleaned.dir, "equis_sample_history.csv"), stringsAsFactors = FALSE) %>% 
  rename_all(tolower) %>% 
  rename(old_ribs_id = sys_loc_code,
         sample_del_grp_equis = sample_delivery_group) %>% 
  mutate(sample_date = parse_date_time(sample_date, c("%m/%d/%Y %I:%M:%S %p", "%m/%d/%Y"))) 
        # Uncomment and check:
         # sample_receipt_date = as.POSIXct(sample_receipt_date, format = "%m/%d/%Y %I:%M:%S %p"))
# select(sample_date, sample_date_test,everything())
  #Looking at repeat SSCs:
  # group_by(sys_sample_code) %>% 
  # mutate(n = n()) %>% 
  # arrange(n)

result.2016 <- read.csv(file.path(tobecleaned.dir, "2016_ribs_result_20201106.csv"), stringsAsFactors = FALSE) %>%
  rename_all(tolower) %>% 
  rename(sample_delivery_group = lab_sdg)
sample.2016 <- read.csv(file.path(tobecleaned.dir, "2016_ribs_sample_20201106.csv"), stringsAsFactors = FALSE,
                        colClasses = c(SiteID="character")) %>%
  rename_all(tolower) %>% 
  rename(old_ribs_id = siteid) %>% 
  mutate(sample_date = parse_date_time(sample_date, c("%m/%d/%Y %H:%M:%S", "%Y/%m/%d %H:%M:%S"))) 

result.2017 <- read.csv(file.path(tobecleaned.dir, "2017_chem_preqaqc_RESULT_complete_2020-11-06.csv"), stringsAsFactors = FALSE) %>%
  rename_all(tolower) %>% 
  rename(sample_delivery_group = lab_sdg)
sample.2017 <- read.csv(file.path(tobecleaned.dir, "2017_chem_preqaqc_SAMPLE_complete_2020-11-09.csv"), stringsAsFactors = FALSE) %>%
  rename_all(tolower) %>% 
  mutate(sample_date = parse_date_time(sample_date, c("%m/%d/%Y %H:%M:%S", "%m/%d/%y %H:%M", "%Y/%m/%d %H:%M:%S")))

result.2018.2019 <- read.csv(file.path(cleaned.dir, "Final_Chemistry_ITS", "20201120_S_CHEM_HISTORY_RESULT.csv"), stringsAsFactors = FALSE) %>%
  rename_all(tolower) %>% 
  setNames(., sub("chr_", "", names(.))) 
sample.2018.2019 <- read.csv(file.path(cleaned.dir, "Final_Chemistry_ITS", "20201023_S_CHEM_HISTORY_SAMPLE.csv"), stringsAsFactors = FALSE) %>%
  rename_all(tolower) %>% 
  setNames(., sub("chs_", "", names(.))) %>% 
  select(-event_smas_sample_date) %>% 
  rename(site_id = event_smas_history_id,
         sample_date = sample_datetime) %>% 
  mutate(sample_date = as.POSIXct(sample_date, format = "%m/%d/%Y %H:%M:%S"))

sites.join <- readxl::read_excel(file.path(tobecleaned.dir, "RIBSchem_historic_BioSiteJoin_2020-04-28.xlsx"))
# sites.master <- readxl::read_excel(file.path(cleaned.dir, "Final_Sites_ITS", "20201117_S_SITE.xlsx"))

```

```{r Join modern site IDs to 2016 and EQuiS data}

# Select only required fields and unique entries 
sites.join.sub <- sites.join %>% 
  select(ADJUST_RIBS_ID, SBU_ID) %>% 
  rename(site_id = SBU_ID,
         old_ribs_id = ADJUST_RIBS_ID) %>%
  distinct() 
  # Looking at single set of duplicates:
#   group_by(ADJUST_RIBS_ID, SBU_ID) %>%
#   filter(n()>1)
        #found that one set of dups was simplified to a single SBU ID 

sample.equis <- sample.equis %>% 
  left_join(sites.join.sub, by = "old_ribs_id") %>% 
  select(site_id, old_ribs_id, everything()) %>% 
  # Add site ID for dup missing sys_loc_code
  mutate(site_id = ifelse(sys_sample_code == "OTER-10.8 DUP", "03-OTER-10.8", site_id))

# To-do later: determine/add site IDs for remaining QC samples
# sample.equis.na <- sample.equis %>%
#   filter(is.na(site_id))

sample.2016 <- sample.2016 %>% 
  left_join(sites.join.sub, by = "old_ribs_id") %>% 
  select(site_id, everything(), -old_ribs_id)
# sample.2016.na <- sample.2016 %>%
#   filter(is.na(site_id))

rm(sites.join, sites.join.sub)
```


# Provisional block for data requests (discontinued) 
Exporting table for fulfulling data requests in the meantime until data is fully cleaned ## 
Select useful fields, join, remove QC samples, and export data 

##Use bits and pieces of this block for final data cleaning below

```{r, eval = FALSE}
# Join sample and result files and select only needed fields
dr.equis.join <- sample.equis %>% 
  left_join(result.equis, by = "sys_sample_code") %>% 
  select(site_id, sample_date, sys_sample_code, sample_matrix_code, sample_type_code, sample_source, chemical_name, fraction, result_value, result_unit, lab_qualifiers, interpreted_qualifiers, validator_qualifiers, method_detection_limit, reporting_detection_limit, quantitation_limit, detection_limit_unit) %>% 
  rename(validator_qualifiers_equis = validator_qualifiers,
         sample_type_code_combined = sample_type_code)
dr.2016.join <- sample.2016 %>% 
  left_join(result.2016, by = c("sys_sample_code", "sample_delivery_group")) %>% 
  select(site_id, sample_date, sys_sample_code, sample_matrix_code, sample_type_code, sample_source, chemical_name, fraction, result_value, result_unit, lab_qualifiers, interpreted_qualifiers, validator_qualifiers, method_detection_limit, reporting_detection_limit, quantitation_limit, detection_limit_unit) %>% 
  rename(sample_type_code_combined = sample_type_code)
dr.2017.join <- sample.2017 %>% 
  left_join(result.2017, by = c("sys_sample_code", "sample_delivery_group")) %>% 
  select(site_id, sample_date, sys_sample_code, sample_matrix_code, dec_sample_type, sample_type_code, sample_source, chemical_name, fraction, result_value, result_unit, lab_qualifiers, interpreted_qualifiers, validator_qualifiers, method_detection_limit, reporting_detection_limit, quantitation_limit, detection_limit_unit) %>% 
  rename(lab_sample_type_cde = sample_type_code,
         dec_sample_type_cde = dec_sample_type)

# Rename new data fields to match old (since there are more of the latter) 
sample.2018.2019.rn <- sample.2018.2019 %>% 
  rename(sys_sample_code = sys_sample_cde,
         sample_matrix_code = sample_matrix_cde)
result.2018.2019.rn <- result.2018.2019 %>% 
  rename(sys_sample_code = sys_sample_cde,
         lab_qualifiers = lab_qual,
         validator_qualifiers = validator_qual,
         method_detection_limit = method_detect_limit,
         reporting_detection_limit = reporting_detect_limit)
dr.2018.2019.join <- sample.2018.2019.rn %>% 
  left_join(result.2018.2019.rn, by = c("sys_sample_code", "sample_del_grp")) %>% 
  select(site_id, sample_date, sys_sample_code, sample_matrix_code, dec_sample_type_cde, lab_sample_type_cde, sample_source, chemical_name, fraction, result_value, result_unit, lab_qualifiers, validator_qualifiers, validator_qual_expln, method_detection_limit, reporting_detection_limit, quantitation_limit, detection_limit_unit)

# Join all data years
chem.2001to2019 <- bind_rows(dr.equis.join, dr.2016.join, dr.2017.join, dr.2018.2019.join) %>% 
  select(site_id, sample_date, sys_sample_code, sample_source, sample_matrix_code, dec_sample_type_cde, lab_sample_type_cde, sample_type_code_combined,
         chemical_name, fraction, result_value, result_unit, lab_qualifiers, interpreted_qualifiers, validator_qualifiers, validator_qual_expln, everything()) %>% 
  mutate(sample_source = tolower(sample_source),
         chemical_name = tolower(chemical_name)) %>% 
  # Fix fraction naming
    mutate(fraction = case_when(
    fraction == "T" ~ "TOTAL",
    fraction == "D" ~ "DISSOLVED",
    TRUE ~ fraction)) 

# Filter to remove QC samples and just retain normal samples.
# First look at what needs to be removed
chem.2001to2019.unique <- lapply(chem.2001to2019, unique)
chem.2001to2019.noqc <- chem.2001to2019 %>% 
  filter(!(sample_matrix_code %in% "WQ"),
         !(sample_source %in% "lab"),
         !(dec_sample_type_cde %in% c("DUP", "EB", "FB", "TB")),
         !(sample_type_code_combined %in% c("FIELD MSR/OBS", "FB", "DUP", "MS", "MSD", "TB", "MB", "EB", "FD", "LCS", "LCSD", "LR")))

# Review unique sample types to ensure all QC types removed
chem.2001to2019.noqc.unique <- lapply(chem.2001to2019.noqc, unique)

# Export both versions with and without QC
# write.table(chem.2001to2019, "C:/Data/Data_requests/smas_data_requests/chem_source/SMAS_chem_2001-2019_PROVIS_FORDATAREQUESTS_20201203.csv",
#             sep=",", row.names = FALSE, na = "")
# 
# write.table(chem.2001to2019.noqc, "C:/Data/Data_requests/smas_data_requests/chem_source/SMAS_chem_noQC_2001-2019_PROVIS_FORDATAREQUESTS_20201203.csv",
#             sep=",", row.names = FALSE, na = "")

```

## Misc data requests from provisional block above (no longer served here; see Rproj smas_data_requests)
Subsetting for Tin Brook request (completed before subset with no QA was generated above)
```{r, eval = FALSE}
chem.2001to2019.tinbrook <- chem.2001to2019 %>% 
  filter(grepl("*TINW", site_id)) %>% 
  mutate(year = substr(sample_date, 0, 4)) %>% 
  filter(year %in% c("2018", "2019")) %>% 
  filter(dec_sample_type_cde %in% "N",
         sample_source %in% "field",
         !(validator_qualifiers %in% "R")) %>% 
  # Omit two records that should have been rejected as a result of RPD equation error
  filter(!(sys_sample_code %in% "13-TINW-0.5-08152018-W" & chemical_name %in% "zinc")) %>% 
  filter(!(sys_sample_code %in% "13-TINW-0.5-10022018-W" & chemical_name %in% "nitrogen, ammonia (as n)")) %>% 
  select(-c(sample_type_code_combined, interpreted_qualifiers, validator_qualifiers_equis, year))

# write.table(chem.2001to2019.tinbrook, 
#             file = file.path("C:/Users/gmlemley/New York State Office of Information Technology Services/SMAS - Data requests/Single_Data_Requests",
#                                                        "TinBrook_chem_simple_20201201.csv"),
#             sep=",", row.names = FALSE, na = "")

```
Subsetting clean chloride data for Charlie, 2020-11-09 (completed before subset with no QA was generated above)
```{r, eval = FALSE}
chem.2001to2019.filter <- chem.2001to2019 %>% 
# Filter out unjoined, lab WQ, and field obvservation records
  filter(!is.na(chemical_name),
         !(sample_matrix_code %in% "WQ"),
         !(sample_source %in% "lab"),
         (sample_type_code_combined %in% c("N", "FB", "DUP", "EB", "FD", NA)))

chem.2001to2019.lab <- chem.2001to2019.filter %>% 
  filter(sample_source == "lab")
# > unique(chem.2001to2019.lab$sample_type_code_combined)
# [1] "SD" "MS" "LR"
# Should these above have the WQ maxtrix code?

# Look at unique records
chem.2001to2019.unique <- lapply(chem.2001to2019.filter, unique)

# Filter out clean, non-QC data for Charlie
chem.2001to2019.cl.charlie <- chem.2001to2019.filter %>% 
  # filter(grepl("*chloride", chemical_name))
  filter(chemical_name %in% "chloride (as cl)") %>% 
  filter(!is.na(site_id),
         !(dec_sample_type_cde %in% c("DUP", "EB")),
         !(sample_type_code_combined %in% c("FB", "DUP", "EB")))

# Review unique sample type codes
chem.2001to2019.cl.unique <- lapply(chem.2001to2019.cl.charlie, unique)

# Double check that no years are missing from filtering funky sample type code conventions
chem.2001to2019.cl.charlie.year <- chem.2001to2019.cl.charlie %>% 
  mutate(year = substr(sample_date, 0, 4)) %>% 
  select(year) %>% 
  distinct()

# write.table(chem.2001to2019.cl.charlie, file = file.path(root.dir, "", "chem.2001to2019.cl.N.Charlie.20201109.csv"),sep=",", row.names = FALSE, na = "")

```


# Required edits prior to joining datasets

```{r Remove equis field data}
# Split result table into -FS and non-FS data to start
result.equis.lab <- result.equis %>% 
  filter(!grepl("*FS", sys_sample_code))
result.equis.field <- result.equis %>% 
  filter(grepl("*FS", sys_sample_code))

# Look at unique "FS" records
# > sort(unique(result.equis.field$chemical_name))
# [1] "Coliform"             "Dissolved Oxygen"     "Fecal Coliform"       "pH"                   "pH (Water)"           "Specific Conductance"
# [7] "Temperature"          "Turbidity"  

### From FS results, KEEP ONLY: Turbidity, Coliform, Fecal Coliform
result.equis.field.keep <- result.equis.field %>% 
  filter(chemical_name %in% c("Turbidity", "Coliform", "Fecal Coliform"))

# Take those removed and set aside for later
result.equis.field.archive <- result.equis.field %>% 
  filter(!(chemical_name %in% c("Turbidity", "Coliform", "Fecal Coliform")))

# Looking into unique lab (non-FS) records:
# > unique(result.equis.lab$lab_name_code)
#  [1] "COLUMBIA"    "GHM"         ""            "VERONA"      "CONVERSE"    "BENDER"      "WESTCHESTER" "H2M"         "MICROBAC"    "AP"         
# [11] "ENDYNE"      "NYSDEC"      "DARRIN"      "CASROCH"     "MOHAWK"      "NYSDOH"      "ECOTEST"     "ALCHEMY"     "CERT"        "LOZIER_NY"  
# [21] "ERIE CTY"    "BUCK"        "FGS"         "ALSRNY"
# > sort(unique(result.equis.lab$chemical_name))
#  [1] "1,2-Dichloroethane-D4"                        "Alkalinity, Total (As CaCO3)"                 "Aluminum"                                    
#  [4] "Arsenic"                                      "Bromodichloromethane"                         "Cadmium"                                     
#  [7] "Calcium"                                      "Chloride (As Cl)"                             "Chlorodibromomethane"                        
# [10] "Chloroform"                                   "Chloromethane"                                "Chlorophyll a"                               
# [13] "Coliform"                                     "Conductivity"                                 "Copper"                                      
# [16] "Dibromochloromethane"                         "Dissolved Organic Carbon"                     "Dissolved Oxygen"                            
# [19] "Fecal Coliform"                               "Fluoride"                                     "Hardness (As CaCO3)"                         
# [22] "Iron"                                         "Lead"                                         "Magnesium"                                   
# [25] "Manganese"                                    "Mercury"                                      "Methylene Chloride"                          
# [28] "Nickel"                                       "Nitrogen"                                     "Nitrogen, Ammonia (As N)"                    
# [31] "Nitrogen, Kjeldahl, Total"                    "Nitrogen, Nitrate-Nitrite"                    "Nitrogen, Nitrate (As N)"                    
# [34] "Nitrogen, Nitrite"                            "Nitrogen, Nitrite + Nitrate"                  "p-Bromofluorobenzene"                        
# [37] "pH"                                           "pH (Water)"                                   "Phenolics, Total Recoverable"                
# [40] "Phosphate Ion"                                "Phosphorus"                                   "Phosphorus, Dissolved Orthophosphate (As P)" 
# [43] "Potassium"                                    "Silver"                                       "Sodium"                                      
# [46] "Specific Conductance"                         "Sulfate (As SO4)"                             "Temperature"                                 
# [49] "Tetrachloroethylene (PCE)"                    "Toluene-D8"                                   "Total Carbon"                                
# [52] "Total Dissolved Solids (Residue, Filterable)" "Total Organic Carbon"                         "Total Solids"                                
# [55] "Total Suspended Solids"                       "Total Volatile Solids"                        "Trichloroethylene (TCE)"                     
# [58] "Turbidity"                                    "Vinyl Chloride"                               "Zinc" 

result.equis.lab.archive <- result.equis.lab %>% 
  filter(lab_name_code %in% "NYSDEC" |
           chemical_name %in% "Dissolved Oxygen")

result.equis.lab.keep <- result.equis.lab %>% 
  filter(!(lab_name_code %in% "NYSDEC")) %>% 
  filter(!(chemical_name %in% "Dissolved Oxygen"))
  

result.equis.keep <- bind_rows(result.equis.field.keep, result.equis.lab.keep)
result.equis.archive <- bind_rows(result.equis.field.archive, result.equis.lab.archive)

rm(result.equis.field.keep, result.equis.lab.keep, result.equis.field.archive, result.equis.lab.archive)

# result.equis.lab.keep_filt <- result.equis.lab.keep %>% 
#   filter(chemical_name %in% "Temperature")
# 
# result.equis.lab.DO.unique <- lapply(result.equis.lab.DO, unique)


# Pull matching records from SAMPLE table for each, and set aside sample records with no matches.

sample.equis.keep <- sample.equis %>% 
  filter(sys_sample_code %in% result.equis.keep$sys_sample_code)

sample.equis.archive <- sample.equis %>% 
  filter(sys_sample_code %in% result.equis.archive$sys_sample_code)

# SAMPLE records for which no RESULT records match
sample.equis.orphan <- sample.equis %>% 
  filter(!(sys_sample_code %in% result.equis.archive$sys_sample_code) & 
           !(sys_sample_code %in% result.equis.keep$sys_sample_code))

# Export data identified as field data and orphaned
# write_csv(result.equis.archive, file.path(datamod.dir, "to_be_cleaned", "field_equis", "equis_chem_fielddata_RESULT_20200122.csv"))
# write_csv(sample.equis.archive, file.path(datamod.dir, "to_be_cleaned", "field_equis", "equis_chem_fielddata_SAMPLE_20200122.csv"))
# write_csv(sample.equis.orphan, file.path(tobecleaned.dir, "equis_chem_SAMPLE_noMatchingResults_omittedFromDB_20200122.csv"))

rm(sample.equis.archive, result.equis.archive, sample.equis.orphan)
rm(result.equis.lab, result.equis.field)
rm(sample.equis, result.equis)

# Test join:
# equis.join <- left_join(sample.equis.keep, result.equis.keep, by = "sys_sample_code")
# # See if there are records in each table that don't have matches in the other
# sample.equis.nomatch <- anti_join(sample.equis.keep, result.equis.keep, by = "sys_sample_code")
# result.equis.nomatch <- anti_join(result.equis.keep, sample.equis.keep, by = "sys_sample_code")

```

```{r Clean up 2016 WG/Mercury join issue}
# Investigate WG issues
sample.2016.WG <- sample.2016 %>% 
  filter(sample_matrix_code %in% "WG")
result.2016.WG <- result.2016 %>% 
  filter(lab_matrix_code %in% "WG")

# Check if WG SSCs are duplicated in sample table
sample.2016.WG.SSC <- sample.2016.WG %>% 
  pull(sys_sample_code)
sample.2016.dupcheck <- sample.2016 %>% 
  filter(sys_sample_code %in% sample.2016.WG.SSC)
# Answer: YES.

# WGs in sample table to not have SDGs, while WGs in results table do... Problem with joining in DB?

# Test join
join.2016 <- left_join(sample.2016, result.2016, by = "sys_sample_code")
# Result: extra recs created (as expected).
join.2016.2 <- left_join(sample.2016, result.2016, by = c("sys_sample_code", "sample_delivery_group"))

sample.2016.nomatch <- anti_join(sample.2016, result.2016, by = c("sys_sample_code", "sample_delivery_group"))
result.2016.nomatch <- anti_join(result.2016, sample.2016, by = c("sys_sample_code", "sample_delivery_group"))
# Result: 120 WG records did not join. Must populate missing SDGs.

# Join SDGs to WG samples records from result file
result.2016.WG <- result.2016 %>% 
  filter(lab_matrix_code %in% "WG") %>% 
  select(sys_sample_code, lab_matrix_code, sample_delivery_group) %>% 
  rename(sample_matrix_code = lab_matrix_code)

sample.2016 <- sample.2016 %>% 
  left_join(result.2016.WG, by = c("sys_sample_code", "sample_matrix_code")) %>% 
  mutate(sample_delivery_group.x = ifelse(sample_matrix_code %in% "WG", sample_delivery_group.y, sample_delivery_group.x)) %>% 
  rename(sample_delivery_group = sample_delivery_group.x) %>% 
  select(-sample_delivery_group.y)

# Test join again
join.2016 <- left_join(sample.2016, result.2016, by = "sys_sample_code")
# Result: extra recs created (as expected)
join.2016.2 <- left_join(sample.2016, result.2016, by = c("sys_sample_code", "sample_delivery_group"))
sample.2016.nomatch <- anti_join(sample.2016, result.2016, by = c("sys_sample_code", "sample_delivery_group"))
result.2016.nomatch <- anti_join(result.2016, sample.2016, by = c("sys_sample_code", "sample_delivery_group"))

# STILL 32 Hg LAB QC records not joined. To be removed. 
sample.2016 <- sample.2016 %>% 
  anti_join(sample.2016.nomatch)

result.2016 <- result.2016 %>% 
  anti_join(result.2016.nomatch)

# Final join test
join.2016 <- left_join(sample.2016, result.2016, by = "sys_sample_code")
# Result: extra recs created (as expected).
join.2016.2 <- left_join(sample.2016, result.2016, by = c("sys_sample_code", "sample_delivery_group"))
sample.2016.nomatch <- anti_join(sample.2016, result.2016, by = c("sys_sample_code", "sample_delivery_group"))
result.2016.nomatch <- anti_join(result.2016, sample.2016, by = c("sys_sample_code", "sample_delivery_group"))
# Result: Join successful :)

```

## Sample type fields

```{r 2016}

# Review unique entries
sample.2016.unique <- lapply(sample.2016, unique)
# > sample.2016.unique[["sample_type_code"]]
#  [1] "N"   "FB"  "EB"  "LB"  "BS"  "BD"  "LR"  "MS"  "SD"  "MSD"
sample.2017.unique <- lapply(sample.2017, unique)
# > sample.2017.unique[["dec_sample_type"]]
# [1] "N"            "N_DUPPARENT"  "DUP"          "EB"           "LAB_INTERNAL"
# [6] "FB"
sample.2018.2019.unique <- lapply(sample.2018.2019, unique)




sample.2016 <- sample.2016 %>% 
          # replace WG sample_matrix_codes with WS (a handful of Hg samples had this).
  
  
  
  mutate(sample_matrix_code = str_replace(sample_matrix_code, "WG", "WS"),
         # Create dec sample type field and modify as needed.
         dec_sample_type_cde = sample_type_code,
         dec_sample_type_cde = ifelse(sample_source == "lab", "LAB_INTERNAL", dec_sample_type_cde),
         # Replace EB and FB in sample_type_code with N. This is to be lab-only sample types to match suit with new data.
         sample_type_code = ifelse(sample_type_code %in% c("FB", "EB"), "N", sample_type_code)
         ) %>% 
  rename(lab_sample_type_cde = sample_type_code) %>% 
  select(site_id, dec_sample_type_cde, everything())

sample.2016_edit.unique <- lapply(sample.2016_edit, unique)
# > sample.2016_edit.unique[["dec_sample_type_cde"]]
# [1] "N"            "FB"           "EB"           "LAB_INTERNAL"
# Confirmed no DUPs collected in 2016. One sample noted as "QC" in sys_sample_code but just Hg present for parent.

```


```{r EQuIS}
sample.equis.unique <- lapply(sample.equis, unique)

sample.equis_edit <- sample.equis %>% 
  
  
# When overwriting "FIELD MSR/OBS" records in sample_type_code, note this in CHS_COMMENT field.
  

```


## Flag fields

```{r EQuIS}

# Look into lab_qualifiers, interpreted_qualifiers, and validator_qualifiers
# Populate validator_qualifier with historic data flag? (already made one?)

# Remove interpreted_qualifiers field. Make sure not losing anything.

```

```{r 2016 and 2017}

# Populate validator_qualifier with historic data flag? (already made one?)

# Remove interpreted_qualifiers field (both 2016 and 2017). Make sure not losing anything.

```



# Applying pcodes
```{r equis}

# Make sure to apply "source: unknown" pcodes to uncertain records (temp, spcond, pH, "pH, (Water)"...? ; lab_name_codes are blank)

# 31,667 record that could not join pcodes b/c result_unit blank??? Use MDL unit?

```

# Joining datasets

```{r}
# Check for any non-matching fields
setdiff(names(sample.2016), names(sample.2017))
setdiff(names(sample.2017), names(sample.2016))

```









# Flag cleaning

```{r Compare EQuIS flag fields}
result.equis.test <- result.equis %>% 
  mutate(flagdiff = ifelse(lab_qualifiers != interpreted_qualifiers, 1, 0)) %>% 
  select(lab_qualifiers, interpreted_qualifiers, flagdiff, validator_qualifiers, everything()) 
  # distinct(lab_qualifiers, interpreted_qualifiers, .keep_all = TRUE)
```







# LOOK INTO MISSING RESULTS DATA (SAMPLE ROWS PRESENT w NO RESULTS) IN JOIN ABOVE ###



### Old code chunks below ###
Use bits and pieces of these blocks for final data cleaning above.



Join with sites table to add modern site IDs
```{r}

# Load in RIBS crosswalk table. Must use this and not sites table due to duplicate RIBS IDs assigned to singe SBU ids (not reflected in sites table; crosswalk retains duplicates)
ribs.crosswalk <- readxl::read_excel(file.path(root.dir, "ribsid_join/RIBSchem_historic_BioSiteJoin_2020-04-28.xlsx")) %>% 
  rename(sys_loc_code = ADJUST_RIBS_ID) %>% 
  select(sys_loc_code, SBU_ID)

equis.sample.siteids <- left_join(equis.sample, ribs.crosswalk, by = "sys_loc_code") %>% 
  select(SBU_ID, everything())

equis.sample.siteids.nomatch <- equis.sample.siteids %>% 
  filter(is.na(SBU_ID)) %>% 
  mutate(year = format(as.Date(sample_date, format="%m/%d/%Y"),"%Y")) %>% 
  select(year, everything())

missing.ribsids <- unique(equis.sample.siteids.nomatch$sys_loc_code) 

# Import EQuIS locations table to see if these un-crosswalked sites are present 
equis.location <- read_csv(file.path(root.dir, "data/equis_location_history.csv")) %>% 
  rename(LATITUDE = LONGITUDE,
         LONGITUDE = LATITUDE)

equis.location.missingribsids <- equis.location %>% 
  filter(SYS_LOC_CODE %in% missing.ribsids,
         SYS_LOC_CODE != "QA_QC") %>% 
  select(SYS_LOC_CODE, LATITUDE, LONGITUDE, SURF_ELEV, ELEV_UNIT, LOC_NAME, LOC_DESC, LOC_COUNTY_CODE, DEC_REGION, LOC_MAJOR_BASIN, 
         NYS_DRAINAGE_BASIN_CODE, STREAM_MILE) %>% 
  mutate(lat_long = paste0(LATITUDE, "_", LONGITUDE))

# missing.diff <- anti_join(equis.location.missingribsids, equis.location.missingribsids2)

# write_csv(equis.location.missingribsids, file.path(root.dir, "ribsid_join/equis.location.missingribsids.csv"))


```


Join sample and result tables and check for issues
```{r}
# SDG not populated for all records. Must use only sys_sample_code.

equis.join <- left_join(sample.equis, result.equis, by = "sys_sample_code")
# 110 extra records created

# See if there are records in each table that don't have matches in the other
sample.equis.nomatch <- anti_join(sample.equis, result.equis, by = "sys_sample_code")
result.equis.nomatch <- anti_join(result.equis, sample.equis, by = "sys_sample_code")

# Export data in-progress for subsetting for finger lakes basin percentiles for Brian's presentation; 6/12/2020
equis.join.bas7.simple <- equis.join %>% 
  filter(grepl("*07-", SBU_ID)) %>% 
  filter(sample_type_code %in% "N") %>% 
  filter(chemical_name %in% c("Phosphorus", "Nitrogen, Nitrate (As N)", "Turbidity", "Nitrogen")) %>% 
  select(sys_sample_code, SBU_ID, sample_date, chemical_name, fraction, result_numeric, result_unit, interpreted_qualifiers, method_detection_limit, detection_limit_unit)

write_csv(equis.join.bas7.simple, "equis.basin7.N-P-turb.simple.csv")

```

Investigate flag field values and change as needed
```{r}
# Make table of unique flags and test methods of removing special characters
labflags <- equis.result %>% 
  select(lab_qualifiers, interpreted_qualifiers) %>% 
  distinct()

### CHANGE DOUBLE LETTER FLAGS TO SINGLE ##
#casewhen... "KK" to "F" and "LL" to "G"
# As in CHEM_QUALIFIER_LAB table

```


Remove field YSI data from equis data, and lab data from field table, and set aside for binding later
```{r}
# First subset out by results table (where there's more info to deduce field vs lab data) then %in% match to subset samples table

equis.result.field <- equis.result %>% 
  filter(grepl("*FS", sys_sample_code))
equis.result.lab <- equis.result %>% 
  filter(!grepl("*FS", sys_sample_code))

# Looking at unique parameters in field and lab subsets

# > unique(equis.result.field$cas_rn)
# [1] TURB        TEMP        SC          DISS_OXYGEN PH          PHWATER     COLIF       FECCOLIFORM

# > unique(equis.result.lab$cas_rn)
#  [1] TVS         TSO         TDS         PH          HARD        SC          FECCOLIFORM COLIF       TSS         TEMP        DISS_OXYGEN TOC        
# [13] 14265-44-2  7723-14-0   14797-65-0  NO3NO2N     14797-55-8  KN          7664-41-7   ALK         16887-00-6  14808-79-8  16984-48-8  7439-97-6  
# [25] 7439-92-1   7440-02-0   7440-43-9   7440-50-8   7429-90-5   7439-89-6   7439-95-4   7439-96-5   7440-09-7   7440-23-5   7440-66-6   7440-70-2  
# [37] TURB        75-27-4     79-01-6     TOTPHEN     124-48-1    127-18-4    67-66-3     74-87-3     75-01-4     75-09-2     7440-22-4   7440-38-2  
# [49] 7727-37-9   PHWATER     121-48-1    DOC         17060-07-0  2037-26-5   460-00-4    PDORTHO     7440-44-0   COND        ARC-NO3NO2N 479-61-8 


### Cleaning up field data

# Subset fecal data from field for binding to lab later
equis.result.lab.fecal <- equis.result.field %>% 
  filter(grepl("COLIF|FECCOLIFORM", cas_rn))
# Remove fecal data from field
equis.result.field <- equis.result.field %>% 
  filter(!grepl("COLIF|FECCOLIFORM", cas_rn))

# Subset turbidity data from field for binding to lab later
equis.result.lab.turb <- equis.result.field %>% 
  filter(grepl("TURB", cas_rn))
# Remove turbidity data from field
equis.result.field <- equis.result.field %>% 
  filter(!grepl("TURB", cas_rn))

# Subset SC data to invesigate and confirm if field-derive or lab-derived
equis.result.field.SC <- equis.result.field %>% 
  filter(grepl("SC", cas_rn))
rm(equis.result.field.SC)
  #conclusion: no indiciation that any records are lab data.

# Subset PH data to invesigate and confirm if field-derive or lab-derived
equis.result.field.PH <- equis.result.field %>% 
  filter(grepl("PH|PHWATER", cas_rn))
rm(equis.result.field.PH)
  #conclusion: no indiciation that any records are lab data.

# Subset temp data to invesigate and confirm if field-derive or lab-derived
equis.result.field.temp <- equis.result.field %>% 
  filter(grepl("TEMP", cas_rn))
rm(equis.result.field.temp)
  #conclusion: no indiciation that any records are lab data.


### Cleaning up lab data

# Subset DO data from lab for binding to field later
equis.result.field.DO <- equis.result.lab %>% 
  filter(grepl("DISS_OXYGEN", cas_rn))
# Remove DO data from lab
equis.result.lab <- equis.result.lab %>% 
  filter(!grepl("DISS_OXYGEN", cas_rn))

# Subset SC data to invesigate and confirm if field-derive or lab-derived
equis.result.lab.SC <- equis.result.lab %>% 
  filter(grepl("SC", cas_rn))
rm(equis.result.lab.SC)
  #conclusion: no indiciation that any records are field data.

# Subset COND data to invesigate and confirm if field-derive or lab-derived
equis.result.lab.COND <- equis.result.lab %>% 
  filter(grepl("COND", cas_rn))
rm(equis.result.lab.COND)
  #conclusion: All confirmed lab data.

# Subset PH data to invesigate and confirm if field-derived or lab-derived
equis.result.lab.PH <- equis.result.lab %>% 
  filter(grepl("PH|PHWATER", cas_rn))
rm(equis.result.lab.PH)
  #conclusion: no indiciation that any records are field data.

# Subset TEMP data to invesigate and confirm if field-derive or lab-derived
equis.result.lab.TEMP <- equis.result.lab %>% 
  filter(grepl("TEMP", cas_rn))
rm(equis.result.lab.TEMP)
  #conclusion: no indiciation that any records are field data.


### Overall conclusion: 2008 data for crossover params (PH, PHWATER, COND, SC, TEMP) looks suspcicious and should be investigated when all data is together. Move ahead with current designations in data.

```

Bind orphaned field and lab data to their respective tables and create their repsective sample tables
```{r}

equis.result.field <- bind_rows(equis.result.field, equis.result.field.DO)

equis.result.lab <- bind_rows(equis.result.lab, equis.result.lab.fecal, equis.result.lab.turb)

rm(equis.result.field.DO, equis.result.lab.fecal, equis.result.lab.turb)

# Create lab and field sample tables from existing results
 equis.sample.field <- equis.sample[equis.sample$sys_sample_code %in% equis.result.field$sys_sample_code, ]
#   check to make sure length is same as unique sys_sample_codes
#     > length(unique(equis.result.field$sys_sample_code))
#     [1] 5564
 
 equis.sample.lab <- equis.sample[equis.sample$sys_sample_code %in% equis.result.lab$sys_sample_code, ]
#   check to make sure length is same as unique sys_sample_codes
#     > length(unique(equis.result.lab$sys_sample_code))
#     [1] 15195
 
# Export field data for appending to appropirate field table
 
```

Investigate field name matches between equis and 2016 data
### SORT OUT 2016 DUPLICATE SAMPLE NAME ISSUES BEFORE PROCESSING BELOW DATA ###
### See fixing_2016_chem.Rmd ###
```{r}
# Get column names and see which match
names_equis.result <- names(equis.result)
names_result.2016 <- names(result.2016)

result.unmatch_cols.eq <- names_equis.result[!names_equis.result %in% names_result.2016]
# Output: > result.unmatch_cols.eq
# [1] "test_comment"        "result_text"         "result_numeric"      "results_historic_id" "facility_name"  

# "test_comment" can be ignored (all NAs)
# "result_text" can be ignored (identical to result_numeric) 
# "results_historic_id" and "facility_name" can be ignored, as these are equis fields.

result.unmatch_cols.2016 <- names_result.2016[!names_result.2016 %in% names_equis.result]
# Output: > result.unmatch_cols.2016
# [1] "comment"      "result_value" 
# "comment" can be ignored (all NAs)
# "result_value" will become "result_numeric"


names_equis.sample <- names(equis.sample)
names_sample.2016 <- names(sample.2016)

sample.unmatch_cols.eq <- names_equis.sample[!names_equis.sample %in% names_sample.2016]
# Output: > sample.unmatch_cols.eq
# [1] "sample_method" "comments"      "facility_name"
# "sample_method" 

sample.unmatch_cols.2016 <- names_sample.2016[!names_sample.2016 %in% names_equis.sample]
# > sample.unmatch_cols.2016
# [1] "sampling_technique" "comment"            "siteid"
# "sampling_technique" is blank and will be populated with "SOP201" and append to equis "sample_method""
# "comment" is blank
# "siteid" will be bound with sys_loc_code

```

Populate fields as needed and rename to match 2016 and equis data
```{r}

result.2016 <- result.2016 %>% 
  rename(result_numeric = result_value) %>% 
  select(-comment)


sample.2016$sampling_technique <- "SOP201" 

sample.2016 <- sample.2016 %>% 
  select(-c(comment, sys_loc_code)) %>% 
  rename(sampling_method = sampling_technique) %>%
  rename(sys_loc_code = siteid)
  
  
  # as.character(sample.2016$sampling_technique)

```


Rename appropriate columns and bind data
```{r}

```


Rename fields to match new database field names (USE NEW UNIFIED DATA MODEL FIELDS)
```{r}

```

